#import sys
import argparse
from .funcmodules import pi, frankenpc, desktop, shutdownall
def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--pi", help="select raspberry pi to perform command", action="store_true" )
    parser.add_argument("-f", "--frankenpc", help="select frankenpc to perform command", action="store_true")
    parser.add_argument("-d", "--desktop", help="select desktop to perform command", action="store_true")
    parser.add_argument("--shutdown", help="Shutdown all computers", action="store_true")
    args=parser.parse_args()
    if args.pi:
        pi()
    if args.frankenpc:
        frankenpc()
    if args.desktop:
        desktop()
    if args.shutdown:
        shutdownall()

if __name__ == '__main__':
    main()
