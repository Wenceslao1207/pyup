import paramiko
from paramiko import *

class systems():
    def __init__(self, user, ip, password):
        self.user = user
        self.ip = ip
        self.password = password

    def set_user(self, user):
        self.user = user

    def execute_command(self, command):
        paramiko.util.log_to_file('ssh.log') # sets up logging
        client = SSHClient()
        client.load_system_host_keys()
        try:
            client.connect(self.ip, username=self.user , password=self.password)
            (ssh_stdin, ssh_stdout, shh_stderr) = client.exec_command(command)
            for line in  ssh_stdout.readlines():
                print(line)
            #ssh_stdout = ssh_stdout.readlines()
            #for line in ssh_stdout:
            #    print(line)

        except:
            print("Connection failed")
            pass
        client.close()

